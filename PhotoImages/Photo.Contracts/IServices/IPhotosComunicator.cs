﻿using Photos.Adaptor.Contracts;
using System.Threading.Tasks;

namespace Photos.Contracts.IServices
{
    public interface IPhotosComunicator
    {
        PhotoResponse GetPhotoById(string id);
    }
}
