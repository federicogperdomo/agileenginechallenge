﻿using System;

namespace Photos.Adaptor.Contracts
{
    public class PictureDto
    {
        public string Id { get; set; }
        public string Cropped_picture { get; set; }

    }
}
