﻿using System.Collections.ObjectModel;

namespace Photos.Adaptor.Contracts
{
    public class PicturesReponse
    {
        public PicturesReponse()
        {
            pictures = new Collection<PictureDto>();
        }

        public Collection<PictureDto> pictures { get; set; }
        public int page { get; set; }
        public int pageCount { get; set; }
        public bool hasMore { get; set; }
    }
}
