﻿using System;

namespace Photos.Adaptor.Contracts
{
    public class PhotoResponse
    {
        public string Id { get; set; }
        public string Author { get; set; }
        public string Camera { get; set; }
        public string Tags { get; set; }
        public string Cropped_picture { get; set; }
        public string Full_picture { get; set; }
    }
}
