﻿using Photos.Adaptor.Contracts;
using Photos.Neg.Model;
using System;
using System.Collections.Generic;

namespace Photos.Neg.Mappers
{
    public static class PhotoMapper
    {
        //It's better to use a mapper library as auto mapper instead
        public static Photo MapToEntity(PictureDto dto, Photo auxToUpdate)
        {
            if (auxToUpdate == null) auxToUpdate = new Photo();
            auxToUpdate.Id = dto.Id;
            auxToUpdate.Cropped_picture = dto.Cropped_picture;

            return auxToUpdate;
        }


        public static Photo MapToEntity(PhotoResponse dto, Photo auxToUpdate)
        {
            if (auxToUpdate == null) auxToUpdate = new Photo();
            auxToUpdate.Id = dto.Id;
            auxToUpdate.Author = dto.Author ?? string.Empty;
            auxToUpdate.Camera = dto.Camera ?? string.Empty;
            auxToUpdate.Tags = dto.Tags ?? string.Empty;
            auxToUpdate.Cropped_picture = dto.Cropped_picture;
            auxToUpdate.Full_picture = dto.Full_picture;

            return auxToUpdate;
        }

        public static List<PhotoResponse> MapToDto(List<Photo> photos)
        {
            List<PhotoResponse> photosReponse = new List<PhotoResponse>();
            PhotoResponse auxReponse = null;
            foreach (var photo in photos)
            {
                auxReponse = new PhotoResponse();
                auxReponse.Id = photo.Id;
                auxReponse.Author = photo.Author;
                auxReponse.Camera = photo.Camera;
                auxReponse.Tags = photo.Tags;
                auxReponse.Cropped_picture = photo.Cropped_picture;
                auxReponse.Full_picture= photo.Full_picture;

                photosReponse.Add(auxReponse);
            }

            return photosReponse;
        }
    }
}
