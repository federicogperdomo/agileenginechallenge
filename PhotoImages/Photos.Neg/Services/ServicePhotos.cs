﻿using Framework.Controllers;
using Photos.Adaptor.Contracts;
using Photos.Neg.Repositories;
using System.Collections.Generic;
using Photos.Contracts.IServices;
using System;
using System.Diagnostics;

namespace Photos.Neg
{
    public class ServicePhotos : IServicePhotos
    {
        private IPhotoCachedRepository _repoCache;
        private IPhotosComunicator _photoComunicator;
        public ServicePhotos(IPhotoCachedRepository repoCache, IPhotosComunicator photoComunicator)
        {
            _repoCache = repoCache;
            _photoComunicator = photoComunicator;
        }
        
        public void LoadImages()
        {
            Debug.Print($"Initialize. Start => {DateTime.Now.ToString("HH:mm:ss")}");
            var helper = WebApiHelper.Create();
            PicturesReponse picturesReponse = new PicturesReponse();

            int nPage = 0;

            //TODO: To improve performance using multithreading (to iterate pages)
            do
            {
                nPage++;
                picturesReponse =  helper.ExecuteHttp<PicturesReponse>(EVerboHttp.Get, $"images?page={nPage}", helper.ActiveToken, null);

                //TOOD async to avoid continuing process while get and cache the image
                foreach (var picture in picturesReponse.pictures)
                {
                    _repoCache.GetById(picture.Id);
                } 
                
            } while (picturesReponse.hasMore); // && picturesReponse.page < 1);

            Debug.Print($"Initialize. Finish => {DateTime.Now.ToString("HH:mm:ss")}");
        }            
    }
}
