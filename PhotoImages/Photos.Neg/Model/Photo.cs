﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Photos.Neg.Model
{
    public class Photo
    {
        public string Id { get; set; }
        public string Author { get; set; }
        public string Camera { get; set; }
        public string Tags { get; set; }
        public string Cropped_picture { get; set; }
        public byte[] Cropped_picture_Image { get; set; }
        public string Full_picture { get; set; }
        public byte[] Full_picture_Image { get; set; }

        
        //Convert.FromBase64String(nuevasImagenes.First(x => x.Nombre == imagen.Nombre).Image);
        //public virtual string ToBase64()
        //{
        //    string base64 = Convert.ToBase64String(Image);
        //    return base64;
        //}
    }
}
