﻿using Photos.Neg.Model;
using System;
using System.Collections.Generic;
using System.Runtime.Caching;
using System.Threading.Tasks;

namespace Photos.Neg.Repositories
{
    public interface IPhotoCachedRepository
    {
        ObjectCache Cache { get;  }
        Photo GetById(string id);
        List<Photo> GetByTerm(string searchTerm);
    }
}