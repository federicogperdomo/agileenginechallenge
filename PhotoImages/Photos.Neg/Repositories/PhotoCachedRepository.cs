﻿using System;
using System.Linq;
using System.Runtime.Caching;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Photos.Neg.Model;
using Photos.Contracts.IServices;
using Photos.Neg.Mappers;
using System.Net;
using System.IO;
using System.Threading.Tasks;

namespace Photos.Neg.Repositories
{

    public class PhotoCachedRepository : IPhotoCachedRepository
    {
        private readonly ObjectCache _cache;
        private readonly IPhotosComunicator _photoComunicator;
        private readonly IConfiguration _configuration;
        public ObjectCache Cache
        {
            get
            {
                return _cache;
            }
        }
        //public PhotoCachedRepository()
        //{
        //    _cache = new MemoryCache("Pictures");
        //    //_photoComunicator = new IPhotosComunicator();
        //}

        public PhotoCachedRepository(IPhotosComunicator photoComunicator, IConfiguration configuration)
        {
            _cache = new MemoryCache("Pictures");
            _photoComunicator = photoComunicator;
            _configuration = configuration;
        }

        public Photo GetById(string id)
        {
            var value = _cache.Get(id.ToString()) as Photo;

            if (value == null)
            {
               value = LoadPhoto(id);

                if (value != null)
                    _cache.Set(id.ToString(), value, GetPolicy());
            }

            return value;
        }

        public List<Photo> GetByTerm(string searchTerm)
        {
            List<Photo> result = new List<Photo>();
            //MemoryCache.Default


            foreach (var item in Cache)
            {
                Photo photo = item.Value as Photo;
                if (photo == null) continue;
                if (photo.Author.Equals(searchTerm) || photo.Camera.Equals(searchTerm) || photo.Cropped_picture.Equals(searchTerm)
                                    || photo.Full_picture.Equals(searchTerm) || photo.Tags.Contains(searchTerm))
                {
                    result.Add(photo);
                }
            }

            return result;
        }



        #region Utils
        private CacheItemPolicy GetPolicy()
        {
            int refreshTime = int.Parse(_configuration["MySettings:RefreshCache"]);
            return new CacheItemPolicy
            {
                UpdateCallback = CacheItemRemoved,
                SlidingExpiration = TimeSpan.FromMinutes(refreshTime),
            };
        }

        private void CacheItemRemoved(CacheEntryUpdateArguments args)
        {
            if (args.RemovedReason == CacheEntryRemovedReason.Expired || args.RemovedReason == CacheEntryRemovedReason.Removed)
            {
                var id = args.Key;
                var updatedEntity = LoadPhoto(id); ;
                args.UpdatedCacheItem = new CacheItem(id, updatedEntity);
                args.UpdatedCacheItemPolicy = GetPolicy();
            }

        }

        private Photo LoadPhoto(string id)
        {
            var dto =  _photoComunicator.GetPhotoById(id);
            Photo value = PhotoMapper.MapToEntity(dto, null);
            //CompleteBase64Image(value);

            return value;
        }


        /// <summary>
        /// Functionality to convert from URL image => Base64 image
        /// </summary>
        /// <param name="photo"></param>
        #endregion Utils

        #region Unused functions
        private void CompleteBase64Image(Photo photo)
        {

            photo.Cropped_picture_Image = this.GetImage(photo.Cropped_picture);
            photo.Full_picture_Image = this.GetImage(photo.Full_picture);
        }
        private byte[] GetImage(string url)
        {
            Stream stream = null;
            byte[] buf;

            try
            {
                WebProxy myProxy = new WebProxy();
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);

                HttpWebResponse response = (HttpWebResponse)req.GetResponse();
                stream = response.GetResponseStream();

                using (BinaryReader br = new BinaryReader(stream))
                {
                    int len = (int)(response.ContentLength);
                    buf = br.ReadBytes(len);
                    br.Close();
                }

                stream.Close();
                response.Close();
            }
            catch (Exception exp)
            {
                buf = null;
            }

            return (buf);
        }
        #endregion Unused functions
    }
}
