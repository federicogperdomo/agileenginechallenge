﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Globalization;
using System.Net.Http.Headers;
using System.Diagnostics;

namespace Framework.Controllers
{
    public class WebApiHelper
    {
        private static WebApiHelper _helper;
        public static WebApiHelper Create()
        {
            if (_helper == null)
            {
                _helper = new WebApiHelper();
            }
            return _helper;
        }

        public string ActiveToken { get; set; }

        private HttpClient _client;
        private HttpClient Client
        {
            get
            {
                _client = new HttpClient();
                _client.BaseAddress = new Uri("http://interview.agileengine.com");
                return _client;
            }
        }

        public TResponse ExecuteHttp<TResponse>(EVerboHttp verb, string apiService, string accessToken, object request)
        {
            //var response = Activator.CreateInstance<TResponse>();
            object response = new object();
            string headerRequest = null;
            string jsonRequest = null;
            string headerResponse = null;
            string result = null;
            DateTime startDate;
            DateTime endDate;
            HttpResponseMessage jsonResponse = default;
            Exception invocationException = null;

            try
            {
                if (request != null) jsonRequest = Newtonsoft.Json.JsonConvert.SerializeObject(request);//JsonString);
                if (jsonRequest is null)
                {
                    jsonRequest = string.Empty;
                }

                startDate = DateTime.Now;
                try
                {
                    using (HttpClient client = Client)
                    {
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                        headerRequest = client.DefaultRequestHeaders.ToString();
                        switch (verb)
                        {
                            case EVerboHttp.Get:
                                {
                                    using (Task<HttpResponseMessage> taskAsync = client.GetAsync(apiService))
                                    {
                                        jsonResponse = taskAsync.GetAwaiter().GetResult();
                                        using (HttpContent content = jsonResponse.Content)
                                        {
                                            headerResponse = jsonResponse.ToString();
                                            // Get contents of page as a String.
                                            Task<string> resultTaskAsync = content.ReadAsStringAsync();
                                            result = resultTaskAsync.GetAwaiter().GetResult();
                                            // jsonResponse.EnsureSuccessStatusCode()
                                        }
                                    }
                                    Debug.Print($"GET {client.BaseAddress}/{apiService} => {result}");
                                    break;
                                }
                            case EVerboHttp.Post:
                                {
                                    var httpContent = new StringContent(jsonRequest, Encoding.UTF8, "application/json");
                                    using (Task<HttpResponseMessage> taskAsync = client.PostAsync(apiService, httpContent))
                                    {
                                        jsonResponse = taskAsync.GetAwaiter().GetResult();
                                        using (HttpContent content = jsonResponse.Content)
                                        {
                                            headerResponse = jsonResponse.ToString();
                                            Task<string> resultTaskAsync = content.ReadAsStringAsync();
                                            result = resultTaskAsync.GetAwaiter().GetResult();
                                            // jsonResponse.EnsureSuccessStatusCode()
                                        }
                                    }
                                    Debug.Print($"POST {client.BaseAddress}/{apiService}/ {jsonRequest} => {result}");
                                    break;
                                }
                        }
                    }

                    if (jsonResponse.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        // lanzamos la excepcion para el log de errores
                        jsonResponse.EnsureSuccessStatusCode();
                    }
                }
                catch (Exception ex)
                {
                    invocationException = ex;
                    // new System.IO.StreamReader(DirectCast(ex, System.Net.WebException).Response.GetResponseStream(), ASCIIEncoding.ASCII).ReadToEnd()
                }

                endDate = DateTime.Now;
                if (invocationException is null)
                {
                    //var type = response.GetType();
                    response = JsonConvert.DeserializeObject(result, typeof(TResponse), SerializationSettings);
                    //response = Newtonsoft.Json.JsonConvert.DeserializeObject(jsonResponse, response.GetType(), SerializationSettings);
                }
            }
            catch (Exception ex)
            {
                invocationException = ex;
            }
            return (TResponse)response;
        }

        private JsonSerializerSettings _SerializationSettings;
        public JsonSerializerSettings SerializationSettings
        {
            get
            {
                if (_SerializationSettings is null)
                {
                    _SerializationSettings = new JsonSerializerSettings();
                    _SerializationSettings.DateFormatHandling = Newtonsoft.Json.DateFormatHandling.IsoDateFormat;
                    _SerializationSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
                    _SerializationSettings.Culture = CultureInfo.InvariantCulture;
                    _SerializationSettings.MissingMemberHandling = Newtonsoft.Json.MissingMemberHandling.Ignore;
                    var resolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();
                    _SerializationSettings.ContractResolver = resolver;
                }

                return _SerializationSettings;
            }

            set
            {
                _SerializationSettings = value;
            }
        }
    }
}