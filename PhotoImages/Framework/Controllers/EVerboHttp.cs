﻿namespace Framework.Controllers
{
    public enum EVerboHttp
    {
        Get,
        Post,
        Put,
        Delete,
        Patch
    }
}