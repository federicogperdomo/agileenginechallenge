﻿using Framework.Controllers;
using Photos.Contracts.IServices;
using Photos.Adaptor.Contracts;
using System.Threading.Tasks;

namespace Photos.Adaptor.Services
{
    public class PhotosComunicator : IPhotosComunicator
    {
        public PhotoResponse GetPhotoById(string id)
        {
            var helper = WebApiHelper.Create();
            PhotoResponse photoReponse =  helper.ExecuteHttp<PhotoResponse>(EVerboHttp.Get, $"images/{id}", helper.ActiveToken, null);

            return photoReponse;
        }

    }
}
