﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Photos.Adaptor.Contracts;
using Photos.Contracts.IServices;
using Photos.Neg;
using Photos.Neg.Mappers;
using Photos.Neg.Repositories;

namespace PhotoImages.Controllers
{
    [Produces("application/json")]
    //[Route("api/photos")]
    public class PhotosController : BaseController
    {

        private readonly IServicePhotos _servicePhotos;
        private readonly IPhotoCachedRepository _repoCache;
        private readonly IPhotosComunicator _photoComunicator;
        private readonly IConfiguration _configuration;
        public PhotosController(IServicePhotos service, IPhotoCachedRepository repoCache, IPhotosComunicator photoComunicator, IConfiguration configuration)
        {
            _servicePhotos = service;
            _repoCache = repoCache;
            _photoComunicator = photoComunicator;
            _configuration = configuration;

            service.LoadImages();
        }

        [HttpGet("search/{searchTerm}")]
        public ActionResult SearchTerms(string searchTerm) //<Photo>
        {
            List<PhotoResponse> result = new List<PhotoResponse>();
            try
            {

                if  ( string.IsNullOrEmpty( searchTerm) ) return BadRequest("searchTerm is required");
               var photos =  _repoCache.GetByTerm(searchTerm);

                result = PhotoMapper.MapToDto(photos);
            }
            catch (Exception ex)
            {
            }
            return Ok(result);
        }
    }
}