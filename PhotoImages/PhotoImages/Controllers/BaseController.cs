﻿using Microsoft.AspNetCore.Mvc;
using Framework.Auth;
using Framework.Controllers;

namespace PhotoImages.Controllers
{
    public class BaseController : Controller
    {
        public BaseController()
        {
            authRequest request = new authRequest();
            var helper = WebApiHelper.Create();
            authResponse autentication = helper.ExecuteHttp<authResponse>(EVerboHttp.Post, "auth", "", request);
            this.ValidToken = autentication.token;
            helper.ActiveToken = autentication.token;
        }

        public string ValidToken { get; set; }
    }
}
